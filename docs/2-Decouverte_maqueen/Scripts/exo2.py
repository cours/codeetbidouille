from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

couleur = [(139, 0, 255), (46, 43, 95), (0, 0, 255), (0, 255, 0), (255, 255, 0), (255, 127, 0), (255, 0, 0)]
while True:
    for i in range(7):
        ledRGB(4, couleur[i][0], couleur[i][1], couleur[i][2])
        sleep(2000)
    eteintLedRGB(4)
    sleep(100)