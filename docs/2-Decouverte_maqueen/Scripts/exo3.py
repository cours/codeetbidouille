from microbit import *
import neopixel
from lib_maqueen import *
from random import randint



# test si le bus i2c est actif
while not power_is_on():
    pass

debut = running_time() # recupere l'heure en milliseconde actuelle
while running_time() - debut < 5000:
    moteurG(255)
    moteurD(255)
moteurG(0)
moteurD(0)
