from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

while True :
    moteurG(235)
    moteurD(255)
    while mesure_distance() > 20:
        if mesure_distance() < 100:
            allume_led('D')
            allume_led('G')
            sleep(250)
            eteint_led('D')
            eteint_led('G')
            sleep(250)
    top=running_time()
    while running_time()-top < 2000:
        moteurG(-160)
        moteurD(-160)
    moteurG(0)
    moteurD(0)