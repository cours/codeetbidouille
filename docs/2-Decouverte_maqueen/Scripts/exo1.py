from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

while True:
    for i in range(4):
        ledRGB(i, 255 - i * 80, i * 80, 128)
        sleep(1000)
    eteintLedRGB(4)
    sleep(100)
