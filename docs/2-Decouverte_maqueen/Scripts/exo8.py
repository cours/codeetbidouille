from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

vitesseG = 0
vitesseD = 0

def led(vitesseG, vitesseD):
    if vitesseG > vitesseD:
        allume_led('G')
        eteint_led('D')
    elif vitesseG < vitesseD:
        allume_led('D')
        eteint_led('G')
    elif vitesseG < 0 and vitesseD < 0:
        allume_led('D')
        allume_led('G')
    else:
        eteint_led('G')
        eteint_led('D')
    

while True:
    vitesseG = 20
    vitesseD = 0
    led(vitesseG, vitesseD)
    sleep(2000)

    vitesseG = 0
    vitesseD = 20
    led(vitesseG, vitesseD)
    sleep(2000)

    vitesseG = -20
    vitesseD = -20
    led(vitesseG, vitesseD)
    sleep(2000)

    vitesseG = 20
    vitesseD = 20
    led(vitesseG, vitesseD)
    sleep(2000)


        
