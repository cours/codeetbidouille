from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

etat = 0
while True:
    etat = capteurD()
    display.show(str(etat))
    sleep(200)