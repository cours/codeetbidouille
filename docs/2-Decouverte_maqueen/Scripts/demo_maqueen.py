############################################
#   Programme de demonstration  du robot   #
#     Maqueen programme avec  Mu editor    #
#  		  par Philippe LECLERC			   #
#			DRANE NORMANDIE                #
#            janvier 2020                  #
############################################
from microbit import *
from lib_maqueen import *

def stop():
    moteurG(0)
    moteurD(0)

# test si le bus i2c est actif
while not power_is_on():
    pass

while True :
    if button_a.was_pressed():
        #compte a rebours
        for i in range(3,0,-1):
            display.show(i)
            sleep(1000)
        display.clear()
        # vitesses pour aller droit
        # le moteur droit est plus faible
        moteurG(235)
        moteurD(255)
        # on avance jusqu'a un obstacle
        # les leds clignotent si on est a moins d'un metre
        while mesure_distance() > 20:
            if mesure_distance() < 100:
                allume_led('D')
                allume_led('G')
                sleep(250)
                eteint_led('D')
                eteint_led('G')
                sleep(250)
        stop()
        # un petit coup de sirene de police
        top=running_time()
        while running_time()-top < 2000:
            sirene()
        # on recule en emettant un son à chaque ligne noire franchie
        moteurG(-160)
        moteurD(-160)
        top=running_time()
        while running_time()-top < 4000:
            if capteurD() == 0 or  capteurG() == 0:
                son(880,500)
        stop()
        # on joue avec les leds RGB
        for i in range(4):
            ledRGB(i,255 - i * 80, i * 80,128)
            sleep(1000)
        eteintLedRGB(4)
        sleep(500)
        # on commande les 4 leds en meme temps
        for i in range(6):
            ledRGB(4,255,0,0)
            sleep(300)
            ledRGB(4,0,255,0)
            sleep(300)
            ledRGB(4,0,0,255)
            sleep(300)
        eteintLedRGB(4)
        # On tourne sur place
        moteurG(100)
        moteurD(-100)
        sleep(1300)
        stop()
        sleep(500)
        moteurG(-100)
        moteurD(+100)
        sleep(1350)
        stop()
        sleep(500)
        # on fait un huit
        moteurD(25)
        moteurG(200)
        sleep(5000)
        moteurD(250)
        moteurG(25)
        sleep(5300)
        stop()
# si on a appuye plusieurs fois sur A, on recommence
