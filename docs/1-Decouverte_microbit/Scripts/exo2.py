from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

for i in range(4):
    display.set_pixel(i, 1, 9)
