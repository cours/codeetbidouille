from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

r1 = 5 # Couleur rouge pale
r2 = 9 # Couleur rouge intense
r3 = 0 # Eteint
x=2
y=2

display.set_pixel(x, y, r2)

while True:
  if button_a.was_pressed():
    display.clear()
    x=x-1
    display.set_pixel(x, y, r1)
  if button_b.was_pressed():
    display.clear()
    x=x+1
    display.set_pixel(x, y, r2)
  if accelerometer.current_gesture() == 'shake':
    while accelerometer.current_gesture() == 'shake':
      attente = 1
    display.clear()
    y=y+1
    display.set_pixel(x, y, r1)
  if pin_logo.is_touched():
    while pin_logo.is_touched():
      attente = 1
    display.clear()
    y=y-1
    display.set_pixel(x, y, r2)
