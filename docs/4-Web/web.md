# Création d'une page Web

## Préambule

Le HTML (Hypertext Markup Language) est un langage de balisage qui sert à représenter les pages Web (WWW : World Wide Web).<br>
Le HTML donne la structure d'une page Web.<br>
Le HTML est généralement associée avec :

- des feuilles de style CSS (Cascading Style Sheets) pour la mise en page (la présentation, le design)
- des scripts en langage JavaScripts pour obtenir un comportement dynamique.

Les langages HTML et CSS font font partie des langages de description (contrairement à JavaScript qui est un langage de programmation).


## Les outils pour coder en langage Html

Le code d'une page HTML est stocké dans un fichier texte avec l'extension `.html`.

Un éditeur de texte est donc nécessaire pour écrire le code source :

- sous Windows, on pourra se servir d'un éditeur avec coloration syntaxique comme NotePad++ (logiciel libre et gratuit téléchargeable [ici](https://notepad-plus-plus.org/downloads/))

Pour interpréter le code HTML, il faut simplement un navigateur Web (c'est-à-dire un client HTTP HyperText Transfer Protocol).<br>
Je vous propose Firefox (logiciel multiplateforme, libre et gratuit, téléchargeable [ici](https://www.mozilla.org/fr/firefox/new/ "Firefox") car il possède par défaut des outils de développement Web très pratiques (sans parler des modules complémentaires).

## Squelette d'une page HTML

Le langage HTML manipule des balises (markup) :

```html
<!DOCTYPE html>

<html>
    <head>
        <!-- en-tête de la page -->
        <!-- encodage des caractères -->
        <meta charset="charset=UTF-8">
        <title>Titre de la page web</title>
    </head>

    <body>
        <!-- corps de la page -->

    </body>
</html>
```
!!! info "Commentaires "
    - Le doctype indique au navigateur la version HTML utilisée par la page.
    - L'en-tête utilisée (élément `<head>`) donne l'encodage des caractères (ici `UTF-8`).
    - La structure de la page est contenue dans l'élément `<body>` : pour l'instant la page est vide.

!!! info "Remarque"
    Avec le navigateur Firefox :

    - pour accéder au code source : Clic droit --> Code source de la page (ou `CTRL+U`)
    - pour accéder aux informations sur la page : Clic droit --> Informations sur la page (ou `CTRL+I`)
    - pour modifier la page en cours (lorsqu'elle est enregistrée sur l'ordinateur) : Clic droit --> Inspecter, faire les modifications, puis `CTRL+S` pour enregistrer.

## Ma première page web

```html
<!DOCTYPE html>

<html>
    <head>
        <!-- en-tête de la page -->
        <meta charset="ASCII">
        <title>Ma première page web en HTML5</title>
    </head>

    <body>
        <!-- corps de la page -->
        <p>Bonjour à  tous !</p>
    </body>
</html>
```

**Question**

Dans NotePad++, copier-coller le code ci-dessus et enregistrez-le dans le fichier `premierepageweb.html` (attention, changer l'extension `.txt` en `.html`)<br>
Ouvrir la page avec Firefox. Le rendu de la page est-il correct? (normalement non, vous devriez voir : Bonjour � tous !)<br>
Quelle grosse erreur faut-il corriger?

## Quelques règles de syntaxe

Toute balise ouverte doit être refermée :

```html
<p>contenu de la balise</p>
```

<p\> est la balise ouvrante, </p\> est la balise fermante.

Certaines balises sont vides (elles n'ont pas de contenu), la fermeture se fait alors immédiatement :

```html
<br>
```
```html
<img src="velo.jpg" alt="vélo">
```
La valeur des attributs doit être entourée de guillements ou d'apostrophes :

```html
<img src='velo.jpg' alt='vélo'>
```
```html
<a href="http://fr.wikipedia.org">Un lien hypertexte vers le site de Wikipédia</a>
```

Les balises doivent être correctement imbriquées :

```html
<p>Cette syntaxe est <strong>bonne</strong></p>
```
```html
<p>Cette syntaxe est <strong>mauvaise</p></strong>
```
## Eléments de type bloc et de type en-ligne (inline)

### Eléments de type bloc

Par défaut, les éléments de type bloc sont affichés par le navigateur avec un saut de ligne au début et à la fin.

Exemples :`<h1>`, `<p>`, `<ul>`, `<table>`, `<hr>`, `<pre>`,`<form>`

le code suivant affichera deux paragraphes, l'un en dessous de l'autre :

```html
<p>Premier paragraphe.</p><p>Deuxième paragraphe.</p>
```

### Eléments de type inline

Les éléments de type inline se placent normalement l'un à côté de l'autre (pas de saut de ligne).

Exemples : `<strong>`, `<em>`, `<a>`, `<img>`, `<sup>`, `<sub>` ...

### Propriétés

Les éléments de type bloc peuvent contenir des éléments de type bloc ou de type inline.

Exceptions : les paragraphe `<p>`  et les titres `<h1>` ... ne peuvent contenir que des éléments inline.<br>
Donc pas de titres dans un paragraphe, pas de paragraphes dans un paragraphe !

Exemple : un élément bloc `<p>` (paragraphe) qui contient un élément inline `<strong>` :
```html
<p>Ceci est <strong>important</strong></p>
```
Les éléments inline ne peuvent contenir que des éléments inline.

*Exemple* : un élément inline `<a>` (lien hypertexte) qui contient un élément inline `<strong>` :
```html
<p><a href="http://fr.wikipedia.org">Un <strong>lien hypertexte</strong> vers le site de Wikipédia</a></p>
```

## Un outil de vérification de la syntaxe

Pour vérifier que votre page Web est conforme aux spécifications HTML5, rendez-vous sur le site du W3C (World Wide Web Consortium) : [http://validator.w3.org](http://validator.w3.org/ "W3C")

Pour une page Web locale (pas encore publiée sur le Web) :

Validate by File Upload -> Check

S'il y a des erreurs, elles vous seront indiquées, avec des explications (en anglais, of course).

Conseil : vérifier et corriger systématiquement vos pages Web avec cet outil.<br>
C'est contraignant au début, mais cela vous fera prendre rapidement de bonnes habitudes.

**Question**

Votre `premierepageweb.html` est-elle valide?

## Une deuxième page web

Vous trouverez [ici](Documents/sources/exemple.html "Balises courantes") des exemples de balises courantes.

**Travail à réaliser**

Etudier le code source de cette page.
Vous pouvez enregistrer la page dans vos documents, puis l'ouvrir et la modifier avec NotePad++.

Vous pouvez également examiner la page avec un des outils de Firefox : Menu -> Développement -> Inspecteur

En synstèse, compléter le tableau (document libre office au nom de [Documents/synthese_html5.odt](Documents/synthese_html5.odt) ):


|Balise|Description|Exemple d'utilisation|
|:-:|:-:|:-:|
|<!-- --\>|Commentaire|<!-- ici un commentaire--\>|
|<p\>|Paragraphe|<p\>Ceci est un paragraphe.</p\>|
|<h1\> à <h6\>||
|<br\>||
|<strong\>||
|<em\>||
|<sub\>||
|<sup\>||
|<ul\> <br> <li\>||
|<table\> <br> <tr\> <br> <th\> <br> <td\>||
|<a\>||
|<img\>||
|<hr\>||

## Un cas particulier : les formulaires

Au début du Web, début des années 1990, l’information était quasi en sens unique, du SERVEUR vers le CLIENT (l’internaute). Il n’existait que peu de données qui transitaient du CLIENT vers le SERVEUR.<br>
L’arrivée des formulaires a changé la donne. Nous sommes maintenant sur une voie à doubles sens ! Le formulaire est là pour recueillir des informations du CLIENT, elles sont ensuite transférées au serveur qui devra les traiter. (les méthodes d’envoi des formulaires GET et POST seront vues dans une autre séquence)

**Analyse d'un formulaire**

Ouvrir le [fichier_formulaire.odt](Documents/fichier_formulaire.odt) et faire ce qui est indiqué. (pensez à enregistrer votre travail).

## Mise en page avec les feuilles de style CSS

Les règles de style CSS permettent de personnaliser les couleurs, de définir les polices de caractères, de positionner les éléments les uns par rapports aux autres, etc...

### Lier des styles à partir d'une feuille séparée

C'est la technique la plus efficace, celle que l'on rencontre dans la plupart des sites Web.

- Code HTML :
```html
<!DOCTYPE html>

<html>
   <head>
       <meta charset="UTF-8">
       <title>Une page web en HTML5</title>
       <link rel="stylesheet" type="text/css" href="style.css">
   </head>

   <body>

       <h2>Les feuilles de style CSS</h2>

       <p>Bonjour à  tous !<br>
       Ceci est un exemple d'utilisation des feuilles de style
       <span class="important">CSS</span> (Cascade Style Sheet).</p>

   </body>
</html>
```
Notes : l'élément `<link>` fait le lien avec la feuille de style CSS (ici le fichier externe syle.css)<br>
L'attribut class de l'élément `<span>` permet d'utiliser des règles CSS personnalisées.

- Code CSS :
La feuille de style se trouve dans le fichier style.css<br>
La syntaxe du langage CSS est très simple à comprendre :

```CSS
body {
background-color: beige;
font-family: 'Times New Roman';
}
h2 {
text-align: center;
color: brown;
}
p {
margin-left: 100px;
}
/* classe */
.important {
color: blue;
font-weight: bold;
}
```
[Voir le résultat avant mise en forme](Documents/sources/css2.html "CSS")

[Voir le résultat après mise en forme](Documents/sources/css1.html "CSS")

### Travail à réaliser

Modifier la feuille de style style.css de manière à afficher :

- le titre en italique avec la police Arial.
- les paragraphes avec une couleur de fond jaune.

On s'aidera des outils que propose Firefox : Menu -> Développement -> Editeur de styles (ou MAJ+F7)

Vous validerez votre code CSS en vous rendant sur le sitre WC : [http://jigsaw.w3.org/css-validator](http://jigsaw.w3.org/css-validator "CSS validation")

### Créer des cadres pour mettre en page

Ouvrir avec Firefox le fichier [Cadre en HTML](Documents/Cadre_en_css/index_cadre.html), puis ouvrir avec NotePad++, dans le dossier Docuements/Cadre_en_css : index.html et style_cadre.css et analyser la construction de différents cadres dans une page web.


## Exercices

**Exercice 1** : Une galerie de grands noms de l'informatique

Tout le monde connaît Bill Gates, Steve Jobs ou Mark Zuckerberg.

Beaucoup moins célèbres mais tout aussi importants, voici une liste non exhaustive de grands informaticiens, avec un lien vers leurs photos :

- [John Von Neumann 1903-1957](http://upload.wikimedia.org/wikipedia/commons/thumb/d/d6/JohnvonNeumann-LosAlamos.jpg/220px-JohnvonNeumann-LosAlamos.jpg "John Von Neumann")\
- [Grace Hopper 1906-1992](http://www.csupomona.edu/~plin/inventors/images/hoppergrace_big.jpg "Grace Hopper")\
- [Alan Turin 1912-1954](http://a3.idata.over-blog.com/300x347/0/02/64/66/ALBUM---2/ALBUM_3/alan_turing.jpg "Alan Turing")\
- [Dennis Ritchie 1941-2011](http://idelways.developpez.com/news/images/dennis_ritchie.jpg "Dennis Ritchie")\
- [Richard Stallman 1953-](http://lh6.ggpht.com/_znke0ocVW6k/TGLVBIOD08I/AAAAAAAAAHE/a6ckD02690s/stallman.jpg?imgmax=800 "Richard Stallman")\
- [Tim Bernes-Lee 1955-](http://rdonfack.developpez.com/tim-berners-lee.jpg "Tim Bernes-Lee")\
- [Linus Torvalds 1969-](http://www.journaldunet.com/solutions/ssii/selection/10-grandes-figures-de-l-open-source/image/linus-torvalds-osdl-492878.jpg "Linus Torvalds")

1) Avec NotePad++, écrire un code HTML d'une page Web qui ressemble à ceci :

![](Documents/grands_informaticiens.jpg "Grands informaticiens")

On fera en sorte que les images soient toutes de la même hauteur (attribut height).

On pourra utiliser un tableau pour aligner facilement les photos par lignes et colonnes.

Vous trouverez les images dans le dossier "Documents/Photo_informaticiens"

2) Avec une feuille de style CSS, modifier à votre goût la couleur de fond de la page, la police, la couleur du texte, etc.

**Exercice 2** Organisation d'une soirée

Vous organisez une soirée, vous voulez savoir:

- Qui vient? Son nom et son numéro de téléphone portable,
- Est-il est motorisé,
- Sera-t-il accompagné, si oui par combien de personnes (max 3),
- Quelle sera sa contribution à la fête,
- L’ensemble du formulaire sera validé par une soumission sous forme d’image.

Vous pouvez vous inspirer du croquis ci-dessous, mais dans tous les cas avant d’écrire votre code faites un schéma préalable sur le papier pour fixer votre cahier des charges. (prévoir 2 fichiers : un en HTML et un en CSS).

![](Images/soiree.png)
