# Découverte du robot Maqueen

## Le robot Maqueen

Maqueen est un petit robot basé sur une carte micro:bit.

![](Images/maqueen.png)

Il est petit, maniable et facile d'utilisation. Il possède beaucoup de fonctionnalités :

- 2 capteurs de suivi de ligne
- 2 LEDs
- 4 LED RGB neopixel pour éclairage d'ambiance
- 1 capteur de distance ultrason
- 1 buzzer pour effets sonores
- 2 moteurs à engrenage contrôlables séparément par i2c
- 1 alimentation par pack de 3 piles AAA
- 1 capteur infrarouge permettant au robot d'être télécommandé

![](Images/vue_dessus.png)

![](Images/vue_dessous.png)

## Prise en main

Nous allons utiliser une bibliothèque préinstallée sur les cartes Micro:Bit: [lib.maqueen.py](Scripts/lib_maqueen.py)

### Les LEDs RGB

**Rappels sur les couleurs**

Vous pouvez avoir un aperçu [ici](http://morganridel.fr/comprendre-le-codage-des-couleurs-en-informatique)

**L'utilisation de ces LEDs**

Pour utiliser les LEDs RGB, on utilise l'instruction `ledRGB(numero_led, rouge, vert, bleu)`, où `numero_led` est compris entre 0 et 3.<br>
Pour utiliser les 4 LEDs en même temps, on mettra 4 en premier paramètre.

**Exercice 1**

1. Tester le programme suivant:
```python
from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

while True:
    for i in range(4):
        ledRGB(i, 255 - i * 80, i * 80, 128)
        sleep(1000)
    eteintLedRGB(4)
    sleep(100)
```
2. Expliquer ce que fait ce programme.

**Exercice 2**

Ecrire un programme permetttant d'allumer les 4 Leds RGB en même temps et qui affiche les couleurs de l'arc-en-ciel dans l'ordre, en changeant de couleurs toutes les 2 secondes.

### Les moteurs

**Utilisation des moteurs**

Pour utiliser le moteur gauche, on utilisera l'instruction suivante: `moteurG(vitesse)` où la vitesse est comprise entre $-255$ et $255$.<br>
On aura l'équivalent pour le moteur droit avec `moteurD(vitesse)`

**Exercice 3**

1. Tester le programme suivant:
```python
from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

debut = running_time() # recupere l'heure actuelle
while running_time() - debut < 5000:
    moteurG(255)
    moteurD(255)
moteurG(0)
moteurD(0)
```
2. Expliquer ce que fait ce programme.

**Exercice 4**

Ecrire un programme permettant de faire faire un carré (minimum 20 cm de côté) au robot Maqueen.

### Les détecteurs de lignes

Pour utiliser les capteurs de lignes (gauche ou droite) on utilisera l'instruction suivante: `capteurD()` ou `capteurG()` qui renvoie l'état logique du capteur (1 lorsqu'on passe sur du blanc, 0 sinon)

**Exercice 5**

1. Tester le programme suivant en posant le robot sur un support blanc puis sur un support noir ou d'une autre couleur **non réfléchissante**:
```python
from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

etat = 0
while True:
    etat = capteurD()
    display.show(str(etat))
    sleep(200)
```
2. Expliquer ce que fait ce programme.

**Exercice 6**

Ecrire un programme permettant d'afficher sur la carte Micro:Bit l'état des deux capteurs.<br>
Par exemple, si le capteur de gauche est sur un objet blanc mais pas le capteur droit, afficher la lettre G.

**Exercice 7**

Le but ici est d'utiliser les capteurs de ligne et les moteurs pour faire un robot suiveur de ligne:

- si les deux capteurs voient du noir (la ligne), le robot avance.
- si un seul capteur voit du noir, c'est que le robot est en train de sortir de la ligne et il faut donc le faire tourner dans le bon sens.

Ecrire un programme permettant d'atteindre ce but.

### Les LEDs gauche et droites

Pour allumer (ou éteindre!) les deux LEDs rouge qui se trouvent à l'avant du robot, on utilisera les instructions suivante:

- Allumer la LED droite: `allume_led('D')`
- Allumer la LED gauche: `allume_led('G')`
- Eteindre la LED droite: `eteint_led('D')`
- Eteindre la LED gauche: `eteint_led('G')`

**Exercice 8**

Ecrire un programme qui allume la LED gauche quand on tourne à gauche, la droite quand on tourne à droite, et les deux quand on recule.

### Le capteur de distance ultrason

Pour utiliser le capteur de distance ultrason (les "yeux" du robot), on aura l'instruction suivante: `mesure_distance()` qui permet de mesurer la distance en centimètre entre le capteur et un éventuel obtacle.

**Exercice 9**

1. Tester le programme suivant:
```python
from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

while True :
    moteurG(235)
    moteurD(255)
    while mesure_distance() > 20:
        if mesure_distance() < 100:
            allume_led('D')
            allume_led('G')
            sleep(250)
            eteint_led('D')
            eteint_led('G')
            sleep(250)
    top=running_time()
    while running_time()-top < 2000:
        moteurG(-160)
        moteurD(-160)
    moteurG(0)
    moteurD(0)
```



### Le buzzer

