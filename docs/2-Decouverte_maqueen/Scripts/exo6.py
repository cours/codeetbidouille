from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

etatG = 0
etatD = 0
while True:
    etatG = capteurG()
    etatD = capteurD()
    if etatG == 1 and etatD == 0:
        display.show(Image("00009:""00009:""00009:""00009:""00009"))

    elif etatG == 0 and etatD == 1:
        display.show(Image("90000:""90000:""90000:""90000:""90000"))

    elif etatG == 0 and etatD == 0:
        display.show(Image.HEART_SMALL)

    else:
        display.show(Image.HEART)
