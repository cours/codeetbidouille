from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

while True :
  lecture = accelerometer.get_x()
  if lecture > 20:
      display.show("D")
  elif lecture < -20:
      display.show("G")
  else:
      display.show("-")
