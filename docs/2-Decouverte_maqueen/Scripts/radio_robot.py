from microbit import *
import radio

radio.config(channel=22)
radio.on()

# appuyer sur les 2 boutons pour demarrer
while not (button_a.is_pressed() and button_b.is_pressed()):
    pass

etat_precedent = False
marche = False

while True:
    # on gere la vitesse du robot en inclinant la carte microbit
    consigne_vitesse = accelerometer.get_y()

    # limitation inclinaison  a ± 765 -> vitesse = ± 255
    vitesse_base = int(max(-765, min(consigne_vitesse, 765)) / 3)
    # on adapte le sens d'inclinaison au sens de marche
    vG = vD = - vitesse_base

    # gestion des boutons
    # Commutation Marche/arret en appuyant sur les 2 boutons
    # detection front montant A.B
    etat_courant = bool(button_a.is_pressed() and button_b.is_pressed())
    if (etat_courant is True and etat_courant != etat_precedent):
        marche = not marche
    etat_precedent = etat_courant

    # modifications des vitesses en fonction des boutons
    if not marche :
        vG = vD = 0
    # bouton A -> tourne a gauche
    elif (button_a.is_pressed() and not button_b.is_pressed()):
        vG = int(vG / 10)
    # bouton B -> tourne a droite
    elif (button_b.is_pressed() and not button_a.is_pressed()):
        vD = int(vD / 10)

    # on envoie les consignes
    datas = "{},{}".format(vG, vD)
    radio.send(datas)
    sleep(100)

