# Initiation au langage Python

**Pour apprendre la programmation en résolvant des énigmes**

Python est un langage de programmation informatique.

On peut choisir différents langages : nous vous proposons de travailler en langage Python.

Se rendre sur le site [France IOI](https://www.france-ioi.org/algo/chapters.php){:target="_blank"}.

Dans le menu, cliquer sur Enseigner > Groupes et classes : rejoindre le groupe intitulé : [Club Code & Bidouille](http://www.france-ioi.org/algo/group.php?idGroup=3636){:target="_blank"}

Un mot de passe vous sera demandé : allende

Complétez votre inscription sur le site. Mémorisez votre identifiant et votre mot de passe (notez-les !) : vous en aurez besoin à chaque connexion sur le site. 

Dans le menu, cliquer ensuite sur Progresser > Cours et problèmes.

Sélectionner le langage Python, ainsi qu'un parcours : le parcours général et le parcours lycée conviennent. Vous pourrez passer de l'un à l'autre à tout moment et ils comportent beaucoup de problèmes communs.
​​

A vous de jouer : les problèmes du niveau 1 vous attendent !