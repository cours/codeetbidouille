from microbit import *

compass.calibrate()

nord = Image("00900:"
             "99999:"
             "00900:"
             "00900:"
             "00900")
sud = Image("00900:"
            "00900:"
            "00900:"
            "99999:"
            "00900")
ouest = Image("09000:"
              "09000:"
              "99999:"
              "09000:"
              "09000")
est = Image("00090:"
            "00090:"
            "99999:"
            "00090:"
            "00090")

while True:
    angle = compass.heading()
    if 80 <= angle <= 100:
        display.show(nord)
    elif 260 <= angle <= 280:
        display.show(sud)
    elif 170 <= angle <= 190:
        display.show(ouest)
    elif (350 <= angle <= 359) or (0 <= angle <= 10):
        display.show(est)
