# Le concours Castor

**Pour découvrir l'algorithmique sans avoir besoin de programmer.**

Le concours Castor est un concours en ligne qui s'adresse aux élèves du CM1 à la Terminale.

Il porte sur des notions liées à la logique et à l'algorithmique. Les énigmes sont posées sous forme de jeux interactifs. Il n'y a pas de code à apprendre pour participer. 

Nous vous proposons en guise d'entraînement de réaliser les épreuves des années précédentes. Chaque épreuve dure 45 min. 

Vous trouverez l'ensemble des épreuves sur cette page : [Plateforme Castor](http://concours.castor-informatique.fr/?tab=home){:target="_blank"}