# Découverte de la carte Micro:Bit

Nous allons nous intéresser dans cette partie à une carte microcontroleur : la carte Micro:Bit.

## Description

![](Images/microbit1.png)

La carte est équipée d’accéléromètres et de gyroscopes réalisés en nano-technologie, qui enregistrent les mouvements selon les 3 axes Pitch, Yaw et Roll :

![](Images/microbit2.png){width=90%}

- X : l'inclinaison de gauche à droite
- Y : l'inclinaison d'avant en arrière
- Z : le mouvement haut et bas

### Les capteurs

La carte est aussi équipée d’un certain nombre de **capteurs**  :

**Vue de face :**

![](Images/microbit4.png){width=40%}

- 1) deux boutons A et B (peuvent être utilisés séparément ou ensemble)
- 6) un logo tactile (peut être utilisé comme un bouton supplémentaire)


**Vue de dos :**

![](Images/microbit3.png){width=40%}

- 1) une antenne radio et Bluetooth (pour "discuter" à plusieurs cartes)
- 2) le processeur et un capteur de température environnante
- 3) une boussole
- 4) un accéléromètre (il mesure les forces en 3 dimensions, y compris la gravité)
- 5) des broches pour connecter d'autres accessoires
- 6) une prise micro USB (pour transférer le programme et alimenter la carte)
- 9) Prise de batterie (pour connecter une batterie externe)
- 10) Puce d'interface USB (sert à flasher le nouveau code)
- 11) Haut-parleur (pour avoir du son)
- 12) Microphone (pour surveiller le son ambiant)
- 13) une led rouge (la carte est alimenté par batterie ou USB)
- 14) une led jaune (elle clignote en cours de chargement du programme, reste allumée si alimention par prise USB)
- 15) Bouton de réinitialisation (appui court) et de marche/arrête (appui long)

### Les actionneurs

La carte est aussi équipée d'**actionneurs** : 

**Vue de face** :

- 2) la carte est munie d'une matrice d'affichage 5x5, soit de 25 LEDs rouges
- 7) LED du microphone (s'allume lorsque la fonction microphone est activée et qu'il y a du bruit)

## Prise en main

### Première utilisation avec le programme pré-enregistrer dans la carte

Branchez la carte à l'ordinateur et suivez les instructions données par la carte. (Attention, cela risque de faire beaucoup de bruit, c'est normal !)

Débranchez maintenant la carte, nous y reviendrons plus tard.

### Premiers programmes

Nous allons par la suite intervenir sur les capteurs de boutons pour actionner les LEDs et le son.

Commençez par aller sur un simulateur : Capytale (application de l'ENT), permet d'accéder au simulateur Micro:Bit de Vittascience.
<br>
Créez une première activité Micro:Bit nommée *Premiers Programmes*.
<br>
Pour passer en mode total Python, cliquez sur ![](Images/micropython.png), puis sur ![](Images/play.png) pour visualiser la carte Micro:Bit.

**Pensez à enregistrer vos différents exercices dans un même fichier `.py` ou alors créez autant d'activités Capytale-Micro:Bit que d'exercices.**

**Exercice 1**

Recopier le programme suivant et l'exécuter dans l'émulateur :

```python
from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

display.set_pixel(0, 0, 9)
```

1. Que fait-il?
2. Modifier la dernière ligne avec : ```display.set_pixel(4, 4, 9)``` et relancer l'émulation.<br>
Vous avez ainsi les coordonnées de la première et de la dernière LED !<br>
On remarquera que le troisième nombre donne l'intensité de la LED :<br>
    - 0 : éteinte
    - 9 : intensité maximale

**Exercice 2**

Créer un programme qui affiche la deuxième ligne, de manière concise.

**Exercice 3**

Recopier le programme ci-dessous :

```python
from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

while True :
  lecture = accelerometer.get_x()
  if lecture > 20:
      display.show("D")
  elif lecture < -20:
      display.show("G")
  else:
      display.show("-")
```

A l'aide de la souris, montez et descendez le curseur (prenez soin de choisir Axe x dans Accéléromètre) et observer ce qu'il se passe.

**Exercice 4**

Pour allumer en une seule fois toutes les leds, on peut utiliser une liste et on applique la commande ```Image()``` et ```show()```. 

1. Par exemple testez :
```python
from microbit import *

display.clear()

boat = Image("05050:"
             "05050:"
             "05050:"
             "99999:"
             "09990")

display.show(boat)
```
2. Ecrire un programme qui affiche les lettres G, D, H ou B lorsque la carte Micro:Bit penche à Gauche, Droite, Haut ou Bas, dans une intensité à définir.

**Exercice 5**

Recopier le programme ci-dessous :

```python
from microbit import *
from time import sleep # Pour faire des pauses

display.clear() # Pour effacer toutes les LEDs

r1 = 5 # Couleur rouge pale
r2 = 9 # Couleur rouge intense
r3 = 0 # Eteint
x=2
y=2

display.set_pixel(x, y, r2)

while True:
  if button_a.was_pressed():
    display.clear()
    x = x - 1
    display.set_pixel(x, y, r1)
  if button_b.was_pressed():
    display.clear()
    x = x + 1
    display.set_pixel(x, y, r2)
  if accelerometer.current_gesture() == 'shake':
    while accelerometer.current_gesture() == 'shake':
      attente = 1
    display.clear()
    y = y + 1
    display.set_pixel(x, y, r1)
  if pin_logo.is_touched():
    while pin_logo.is_touched():
      attente = 1
    display.clear()
    y = y-1
    display.set_pixel(x, y, r2)
```
Utiliser les boutons A, B, Logo et la fonction "Secouer" et observer le résultat sur le la carte, puis faire le lien avec le programme.

**Exercice 6**

Ecrire un programme qui fait se déplacer horizontalement un point de la matrice LEDs grâce aux boutons A et B et qui change d'intensité en recontrant un bord.

## Transférer un programme sur une carte Micro:Bit

Trois solutions s'offre à nous :

- Avec le navigateur Edge, Chrome ou Opera (tout dépendra du pc sur lequel vous travaillez) :\
Depuis le site du simulateur, branchez votre carte Micro:Bit via le cordon USB, puis cliquez sur **Téléverser**.
- Avec le logiciel Mu :\
Après avoir branché votre carte Micro:Bit via le cordon USB, copiez/collez le code python, puis cliquez sur **Flasher**

**Exercice 7**

Testez quelques programmes déjà écrit précédemment directement sur la carte Micro:Bit.

**Exercice 8**

Testez ce programme :
```python
from microbit import *
import music

music.pitch(261, duration=1000)
sleep(5)
music.pitch(293, duration=1000)
sleep(5)
music.pitch(329, duration=1000)
sleep(5)
music.pitch(349, duration=1000)
sleep(5)
music.pitch(392, duration=1000)
sleep(5)
music.pitch(440, duration=1000)
sleep(5)
music.pitch(494, duration=1000)
sleep(5)
music.play(music.DADADADUM, loop=False)
display.scroll("Perdu !", delay=150, wait=True)
```

Il existe plusieurs mélodies :

- Beethoven - Dadadadum ( ```DADADADUM``` )
- Beethoven - Ode to Joy ( ```ODE``` )
- Blues (```BLUES```)
- Chopin - Funeral March ( ```FUNERAL``` )
- Jump down ( ```JUMP_DOWN``` )
- Joplin - Entertainer ( ```ENTERTAINER``` )
- Ringtone ( ```RINGTONE``` )
- Happy Birthday ( ```BIRTHDAY``` )
- Wawawawaa ( ```WAWAWAWAA``` )
- Power Up ( ```POWER_UP``` )
- Bach - Prelude ( ```PRELUDE``` )
- Funk ( ```FUNK``` )
- Wagner - Wedding ( ```WEDDING``` )
- Jump Up ( ```JUMP_UP``` )
- Power Down ( ```POWER_DOWN``` )

**Exercice 9**

La carte Micro:Bit peut aussi faire office de compas.

Testez le code suivant:
```python
from microbit import *

compass.calibrate()

while True:
  if button_a.was_pressed():
    display.scroll(compass.heading())
```

Au lancement du programme, on pourra voir sur la carte "TILT TO FILL SCREEN". Le compas demande à être calibré une première fois.
<br>
Pour cela, il suffit de bouger la carte de manière à allumer toutes les LED en rouge.
<br>
Une fois la carte calibrer, en appuyant sur le bouton A, on peut voir apparaître la valeur du compas.

**Exercice 10**

Ecrire un programme affichant dans la matrice LED une flèche indiquant approximativement le Nord, quelle que soit l'orientation de la carte.


## Résumé des différentes commandes

- Importer la bibliothèque gérant la carte Micro:Bit: `from microbit import *`.
- Importer la bibliothèque gérant le temps: `from time import *`.
- Importer la bibliothèque gérant le son: `import music`.
- Effacer toutes les LEDs: `display.clear()`.
- Afficher un pixel aux coordonnées $(x;y)$ d'intensité lumineuse $i$: `display.set_pixel(x,y,i)`.
- Afficher un caractère en LEDs: `display.show("caractere")`.
- Afficher une phrase qui défile à la vitesse 150: `display.scroll("phrase", delay=150, wait=True)`
- Récupérer les valeurs sur l'axe $x$ par exemple de l'accéleromètre: `accelerometer.get_x()`.
- Calibrer le compas: `compass.calibrate()`
- Faire une action après que le bouton A a été appuyé: `if button_a.was_pressed():`.
- Faire une action lors de l'appuie du bouton A: `if button_a.is_pressed():`.
- Faire une action lorsque la carte est secouée: `if accelerometer.current_gesture() == 'shake:'`.
- Faire une action lors de l'appuie sur le logo: `if pin_logo.is_touched():`.
- Jouer une note de musique de fréquence $x$ Hertz pendant 1 seconde: `music.pitch(x, duration = 1000)`.