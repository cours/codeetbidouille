from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass

etatG = 0
etatD = 0

while True:
    etatD = capteurD()
    etatG = capteurG()
    if etatG == 1 and etatD == 0:
        moteurG(80)
        moteurD(-80)
    elif etatG == 0 and etatD == 1:
        moteurG(-80)
        moteurD(80)
    elif etatG == 0 and etatD == 0:
        moteurG(20)
        moteurD(20)
    else:
        moteurG(0)
        moteurD(0)
