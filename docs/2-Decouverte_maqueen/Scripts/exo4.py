from microbit import *
import neopixel
from lib_maqueen import *
from random import randint

# test si le bus i2c est actif
while not power_is_on():
    pass


for i in range(3):
    debut = running_time() # recupere l'heure actuelle
    while running_time() - debut < 2000:
        moteurG(255)
        moteurD(255)
    debut = running_time() # recupere l'heure actuelle
    while running_time() - debut < 300:
        moteurG(255)
        moteurD(-255)
moteurG(0)
moteurD(0)
debut = running_time() # recupere l'heure actuelle
while running_time() - debut < 2000:
    moteurG(255)
    moteurD(255)
moteurG(0)
moteurD(0)
