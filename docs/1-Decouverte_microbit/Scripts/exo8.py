from microbit import *
import music

music.pitch(261, duration=1000)
sleep(5)
music.pitch(293, duration=1000)
sleep(5)
music.pitch(329, duration=1000)
sleep(5)
music.pitch(349, duration=1000)
sleep(5)
music.pitch(392, duration=1000)
sleep(5)
music.pitch(440, duration=1000)
sleep(5)
music.pitch(494, duration=1000)
sleep(5)
music.play(music.DADADADUM, loop=False)
display.scroll("Perdu !", delay=150, wait=True)
